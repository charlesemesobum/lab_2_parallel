#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
 
int main ()
{
struct tm *time_ptr;
time_t clock;
char tstr[20];
pid_t child_pid;
// printf ("MAIN: process ID is %d ", (int) getpid ());
child_pid = fork ();
if (child_pid != 0) {
printf ("MAIN: process ID is %d ", (int) getpid ());
printf ("MAIN: child process ID is %d ", (int) child_pid);
clock = time(NULL);
time_ptr = localtime(&clock);
strftime(tstr, 20, "%H:%M:%S", time_ptr);
printf ("MAIN: started at %s ", tstr);
int i = 0;
double x = 0.02323;
for (i=0;i<=1999999999;i++) {
x = x * 2;
}
clock = time(NULL);
time_ptr = localtime(&clock);
strftime(tstr, 20, "%H:%M:%S", time_ptr);
printf ("MAIN: stopped at %s ", tstr);
} else {
printf ("CHILD: process ID is %d ", (int) getpid ());
clock = time(NULL);
time_ptr = localtime(&clock);
strftime(tstr, 20, "%H:%M:%S", time_ptr);
printf ("CHILD: started at %s ", tstr);
int i = 0;
double x = 0.02323;
for (i=0;i<=1999999999;i++) {
x = x * 2;
}
clock = time(NULL);
time_ptr = localtime(&clock);
strftime(tstr, 20, "%H:%M:%S", time_ptr);
printf ("CHILD: stopped at %s ", tstr);
}
return 0;
}
