#include <stdio.h>

int main(int argc, char** argv) {

int myPid = getpid();
int myParentPid = getppid();
printf("I'm First program and my PID is: %d \n",myPid);
printf("my parent PID is: %d \n",myParentPid);
printf("===========first fork================\n");
int forkedPID = fork();
printf("new forked process PID is: %d \n",forkedPID);
printf("new forked process parent PID is: %d \n",getppid());
int signal = 8;
//printf("Child try to kill his parent with signal %d \n",signal);
printf("Parent try to kill his child with signal %d \n",signal);
kill(forkedPID,8);
sleep(100);
return 0;
}
